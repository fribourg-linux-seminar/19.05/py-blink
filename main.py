"""
Copyright 2019 Jacques Supcik, HEIA-FR

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import machine
import utime as time

DELAY = const(150)


def main():
    led0_pin = machine.Pin(2, machine.Pin.OUT)
    led0 = machine.Signal(led0_pin, invert=True)

    led1_pin = machine.Pin(16, machine.Pin.OUT)
    led1 = machine.Signal(led1_pin, invert=True)

    while True:
        led0.on()
        led1.off()
        time.sleep_ms(DELAY)
        led0.off()
        led1.on()
        time.sleep_ms(DELAY)


main()
