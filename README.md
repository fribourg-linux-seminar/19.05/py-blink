
# py-blink

MicroPython demo for the [16th Fribourg Linux Seminar](https://www.meetup.com/fr-FR/Fribourg-Linux-Seminar/events/256776987/) : Simple LED blinker.

![py-blink-photo](https://i.imgur.com/2tA0Paw.jpg)
